# Legion::Cache

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/legion/cache`. To experiment with that code, run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'legion-cache'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install legion-cache

## Usage


## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/legion-io/legion-cache/issues


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

require_relative 'cache/version'
require_relative 'cache/settings'

require_relative 'cache/memcached'
require_relative 'cache/redis'

module Legion
  module Cache
    class << self
      include Legion::Cache::Memcached if Legion::Settings[:cache][:driver] == 'dalli'
      include Legion::Cache::Redis if Legion::Settings[:cache][:driver] == 'redis'

      def setup(**opts)
        return Legion::Settings[:cache][:connected] = true if connected?

        return unless client(**Legion::Settings[:cache], **opts)

        @connected = true
        Legion::Settings[:cache][:connected] = true
      end

      def shutdown
        Legion::Logging.info 'Shutting down Legion::Cache'
        close
        Legion::Settings[:cache][:connected] = false
      end
    end
  end
end

# frozen_string_literal: true

require_relative 'lib/legion/cache/version'

Gem::Specification.new do |spec|
  spec.name          = 'legion-cache'
  spec.version       = Legion::Cache::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Legion::Cache integration'
  spec.description   = 'Used to connect Legion with a cache service like memcached'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion-cache'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion-cache/src'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/legion-io/legion-cache/src/master/CHANGELOG.md'
  spec.metadata['wiki_uri'] = 'https://bitbucket.org/legion-io/legion-cache/wiki'
  spec.metadata['bug_tracker_uri'] = 'https://bitbucket.org/legion-io/legion-cache/issues'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '>= 2'
  spec.add_development_dependency 'legion-logging'
  spec.add_development_dependency 'legion-settings'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov', '< 0.18.0'

  spec.add_dependency 'connection_pool', '>= 2.2.3'
  spec.add_dependency 'dalli', '>= 2.7'
  spec.add_dependency 'redis', '>= 4.2'
end
